# hdcc – Haskell Dreche Compiler Collection

This is my personal test compiler, mostly for some custom languages I want to
try out (some DSL, some actual programming languages). The ideas for these are
described in [my other repository](https://gitlab.com/TheDreche/dlang/).

This actually is a rewrite of [rxc](https://gitlab.com/TheDreche/rxc) which was
made because C++ didn't seem to be the right programming language for the job
which I actually just want to get done (ideally without too messy code) and
didn't care about performance at all. I've chosen C++ in the first place as it
was my favourite programming language at that point, where I only knew Python
and C++ (and didn't understand how to create CLI tools using Java, Javscript or
Typescript).

## License

Unless noted otherwise, hdcc is generally licensed under the following
conditions:

Copyright (C) 2023, 2024 Dreche

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.

// vim:textwidth=80:
