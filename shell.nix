{ global_pkgs ? import <nixpkgs> {} }:

let
	pkgs = if builtins.pathExists ./nix/sources.nix
		then import (import ./nix/sources.nix).nixpkgs {config = global_pkgs.config;}
		else global_pkgs;

	stack-wrapped = pkgs.symlinkJoin {
		name = "stack";
		paths = [ pkgs.stack ];
		buildInputs = [ pkgs.makeWrapper ];
		postBuild = ''
			wrapProgram $out/bin/stack \
				--add-flags "--nix --nix-shell-file=nix/stack-integration.nix"
		'';
	};
in pkgs.haskellPackages.shellFor {
	packages = hpkgs: [];
	nativeBuildInputs = with pkgs; [
		cabal2nix
		(haskell-language-server.override {supportedGhcVersions = ["96"];})
		hpack
		niv
		ormolu
		stack-wrapped
	];
	withHoogle = true;
}
