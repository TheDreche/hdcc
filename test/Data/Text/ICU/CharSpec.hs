{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 - 
 - Copyright (C) 2023, 2024  Dreche
 - 
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 - 
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 - 
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}

module Data.Text.ICU.CharSpec (spec) where

import Data.Text.ICU.Char (Bool_ (WhiteSpace, XidContinue), property)
import Test.Hspec (Spec, describe, it, shouldSatisfy)

spec :: Spec
spec = do
  describe "property XidContinue" $ do
    it "sees the ASCII identifier characters as such" $ do
      sequence_ $ flip shouldSatisfy (property XidContinue) <$> "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
  describe "property WhiteSpace" $ do
    it "sees the ASCII whitespace characters as such" $ do
      sequence_ $ flip shouldSatisfy (property WhiteSpace) <$> " \t\r\n"
