{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StandaloneDeriving #-}

module Hdcc.Dml.TypeChecker
  ( Error (..),
    File (File),
    FileIdentifier (FileIdentifier),
    FileExpression (..),
    FileExpressionTyped (FileExpressionTyped),
    checkTypes,
  )
where

import Control.Monad (join)
import Control.Monad.Trans.Class (MonadTrans (lift))
import Control.Monad.Trans.Except (runExcept)
import Control.Monad.Trans.Reader (ReaderT (ReaderT, runReaderT), ask, withReaderT)
import Control.Monad.Trans.Writer (WriterT (runWriterT), tell)
import Data.Foldable (Foldable (toList))
import Data.Functor (($>), (<&>))
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (catMaybes, fromMaybe)
import Data.MonoidMap (MonoidMap)
import qualified Data.MonoidMap as MM
import qualified Data.Set as Set
import Data.Text (Text)
import Data.Tuple (swap)
import Hdcc.Dml.Lexer (TokenType)
import Hdcc.Dml.Link (GlobalIdentifier (), Linked, LinkedT)
import qualified Hdcc.Dml.Link as Link
import qualified Hdcc.Dml.Tokenizer as Tokenizer
import qualified Hdcc.General.Error as Error
import Hdcc.General.Link (fetch)
import qualified Hdcc.General.Link as GLink
import qualified Hdcc.General.Parser as Parser
import Hdcc.General.Position (Span ())
import qualified Hdcc.General.Set as S
import Hdcc.General.TypeChecker (ActionT, mapActionT)
import qualified Hdcc.General.TypeChecker as General
import Prelude hiding (error, lookup, span)

-- Result
data FileIdentifier = FileIdentifier Text Integer deriving (Eq, Ord, Show) -- TODO: Assign these in linker instead

data FileExpression
  = VGlobal GlobalIdentifier
  | VAttribute GlobalIdentifier FileExpressionTyped
  | VBlock (Map FileIdentifier FileExpressionTyped) FileExpressionTyped
  | VFunctionCall FileExpressionTyped FileExpressionTyped
  | VFunctionDefinition FileIdentifier FileExpressionTyped
  | VTypeInstanciation (Map GlobalIdentifier FileExpressionTyped)
  | VLocal FileIdentifier
  deriving (Eq, Ord, Show)

data FileExpressionTyped = FileExpressionTyped FileType FileExpression deriving (Eq, Ord, Show)

data FileType = TCompound (Map GlobalIdentifier FileType) (Maybe FileType) | TFunction FileType FileType
  deriving (Eq, Ord, Show)

newtype File = File FileExpressionTyped deriving (Eq, Ord, Show)

data Error
  = EGlobalMultiple Integer
  | EIO FilePath IOError
  | ETokenizer (Error.Error Tokenizer.Error)
  | EFileMultiparse (Set.Set ([Parser.Correction TokenType], Link.ValuePositioned))
  | EFileExpected Span TokenType TokenType
  | EFileRemove Span
  deriving (Eq, Show)

-- Process
class (Monad m) => MonadAction m where
  aWith :: GlobalIdentifier -> (Link.File -> m v) -> m v
  aLazy :: m v -> m (m v)
  aError :: Error -> m ()

newtype CheckT l m v = CheckT (ReaderT l (WriterT (MonoidMap l (S.Set Error)) (LinkedT l m)) v) deriving (Functor)

deriving instance (Ord l, Monad m) => Applicative (CheckT l m)

deriving instance (Ord l, Monad m) => Monad (CheckT l m)

instance (Ord l) => MonadTrans (CheckT l) where
  lift = CheckT . lift . lift . lift

instance (Ord l, Monad m) => MonadAction (CheckT l m) where
  aWith name process = do
    current <- CheckT ask
    loader <- CheckT $ lift $ lift $ GLink.lookup current name
    content <- CheckT $ lift $ lift $ GLink.fetch loader
    mapCheckT (withReaderT $ const loader) $ process content
  aLazy action = CheckT ask <&> \current -> mapCheckT (withReaderT $ const current) action
  aError e = do
    current <- CheckT ask
    CheckT $ lift $ tell $ MM.singleton current (S.singleton e)

mapCheckT ::
  ( ReaderT l (WriterT (MonoidMap l (S.Set Error)) (LinkedT l m)) v ->
    ReaderT l' (WriterT (MonoidMap l' (S.Set Error)) (LinkedT l' m')) v'
  ) ->
  CheckT l m v ->
  CheckT l' m' v'
mapCheckT f (CheckT c) = CheckT $ f c

checkTypes :: (Ord l) => l -> Linked l (MonoidMap l (S.Set Error), Maybe File)
checkTypes entry = do
  let unCheck (CheckT c) = c
  entry' <- fetch entry
  fmap swap $ runWriterT $ unCheck (typeChecker entry') `runReaderT` entry

-- Implementation
data LazyType m
  = LCompound (m (Map GlobalIdentifier (m (LazyType m)))) (m (Maybe (m (LazyType m))))
  | LFunction (m (LazyType m)) (m (LazyType m))
  | LAny -- Invalid type; just assume it to be correct (an error should already have been given)

-- This does not save the types of the individual expressions as they should be easy to deduce. However, for type checking, the deduced types should be checked against the declared type, if applicable.
data LazyExpression m
  = LGlobal GlobalIdentifier
  | LAttribute GlobalIdentifier (LazyExpression m)
  | LBlock (Map FileIdentifier (LazyExpression m, Maybe (m (LazyType m)))) (LazyExpression m)
  | LFunctionCall (LazyExpression m) (LazyExpression m)
  | LFunctionDefinition FileIdentifier (LazyExpression m)
  | LTypeInstanciation (Map GlobalIdentifier (LazyExpression m))
  | LLocal FileIdentifier

data LazyExpressionTyped m = LazyExpressionTyped (LazyType m) (LazyExpression m)

typeConvertible :: (Monad m) => LazyType m -> LazyType m -> m Bool
typeConvertible LAny _ = pure True
typeConvertible _ LAny = pure True
typeConvertible (LCompound lhs lhs_subtype) (LCompound rhs rhs_subtype) = do
  lhs' <- lhs
  member_compatible <-
    rhs
      >>= fmap and
        . sequence
        . Map.mapWithKey
          ( \member_name member_type -> do
              case Map.lookup member_name lhs' of
                Nothing -> pure False
                Just member_type_lhs -> join $ typeConvertible <$> member_type_lhs <*> member_type
          )
  if member_compatible
    then do
      rhs_subtype >>= \case
        Nothing -> pure True
        Just rhs_subtype' ->
          lhs_subtype >>= \case
            Nothing -> pure False
            Just lhs_subtype' -> join $ typeConvertible <$> lhs_subtype' <*> rhs_subtype'
    else pure False
typeConvertible (LCompound _ _) (LFunction _ _) = pure False
typeConvertible (LFunction _ _) (LCompound _ _) = pure False
typeConvertible (LFunction lhs_parameter lhs_result) (LFunction rhs_parameter rhs_result) = do
  paramconv <- join $ typeConvertible <$> rhs_parameter <*> lhs_parameter
  if paramconv
    then join $ typeConvertible <$> lhs_result <*> rhs_result
    else pure False

-- TODO: Report errors on LAny
expressionType :: (MonadAction m) => LazyExpression m -> m (LazyType m) -- TODO: Generally, switch to ValuePositioned and add a function for evaluating it (in type ValueEvaluated)
expressionType (LGlobal name) = lazyFile name >>= maybe (pure LAny) (>>= expressionType)
expressionType (LAttribute name value) =
  expressionType value >>= \case
    LCompound members _ -> members >>= fromMaybe (pure LAny) . Map.lookup name
    LFunction _ _ -> pure LAny
    LAny -> pure LAny
expressionType (LFunctionCall func _) =
  expressionType func >>= \case
    LCompound _ _ -> pure LAny
    LFunction _ result -> result
    LAny -> pure LAny
expressionType _ = undefined -- TODO

lazyFile :: (MonadAction m) => GlobalIdentifier -> m (Maybe (m (LazyExpression m)))
lazyFile entry = aWith entry $ \(Link.File content) -> do
  loaded_files <-
    catMaybes . toList
      <$> mapM
        ( \e -> case runExcept e of
            Left (Link.EIO _ file_path io_error) -> aError (EIO file_path io_error) $> Nothing
            Right v -> pure $ Just v
        )
        content
  case loaded_files of
    [(loaded_file, errors)] ->
      mapM_ (aError . ETokenizer) errors >> case Set.toList loaded_file of
        [(corrections, parsed_file)] -> fmap Just $ aLazy $ do
          -- TODO: Report corrections
          undefined
        _ -> pure Nothing -- TODO: Report errors ("corrections") and not that the file does not exist
    _ -> pure Nothing -- TODO: Is there an error for files that don't exist?

typeChecker :: (MonadAction m) => Link.File -> m (Maybe File)
typeChecker (Link.File entry) = pure undefined
