{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}

module Hdcc.Dml.Parser
  ( parse,
    BlockContent (BlockContent),
    BlockStatement (BlockStatementConstantDefinition, BlockStatementConstantDeclaration),
    BlockStatementTerminated (BlockStatementTerminated),
    Expression
      ( ExpressionBlock,
        ExpressionFunctionCall,
        ExpressionFunctionDefinition,
        ExpressionFunctionType,
        ExpressionSymbolGlobal,
        ExpressionSymbolLocal,
        ExpressionTypeDefinition,
        ExpressionTypeInstanciation
      ),
    File,
    FunctionDefinitionParameter (FunctionDefinitionParameter),
    GlobalIdentifier (GlobalIdentifier),
    LocalIdentifier (LocalIdentifier),
    NamespaceReference (NamespaceReferenceGlobal, NamespaceReferenceProject),
    NamespaceReferenceSubnamespace (NamespaceReferenceSubnamespace),
    TkBracketCurlyLeft (TkBracketCurlyLeft),
    TkBracketCurlyRight (TkBracketCurlyRight),
    TkBracketRoundLeft (TkBracketRoundLeft),
    TkBracketRoundRight (TkBracketRoundRight),
    TkColon (TkColon),
    TkComma (TkComma),
    TkDot (TkDot),
    TkEquals (TkEquals),
    TkIdentifier (TkIdentifier),
    TkSemicolon (TkSemicolon),
    TypeDefinitionStatement (TypeDefinitionStatementProperty),
    TypeInstanciationStatement (TypeInstanciationStatementProperty),
  )
where

import Control.Monad (msum)
import Data.Text (Text)
import Hdcc.Dml.Lexer (Token)
import qualified Hdcc.Dml.Lexer as Lexer
import qualified Hdcc.Dml.Tokenizer as Tokenizer
import Hdcc.General.Parser
  ( Node,
    NodeData,
    NodeToken,
    addCompoundPostfixes,
    genCompoundParser,
    genListParser,
    genListSeparatedParser,
    genMaybeParser,
    genTokenParser,
  )
import qualified Hdcc.General.Parser as General
import Prelude hiding (filter, map)

-- Type aliases
type Parser = General.Parser Lexer.TokenType Token

-- Token nodes
{- ORMOLU_DISABLE -}
data TkBracketCurlyLeft = TkBracketCurlyLeft deriving (Eq, Ord, Show)
instance NodeData TkBracketCurlyLeft
parseTkBracketCurlyLeft :: Parser (NodeToken TkBracketCurlyLeft)
parseTkBracketCurlyLeft = genTokenParser Lexer.TkTBracketCurlyLeft $ \case Lexer.TkBracketCurlyLeft -> Just TkBracketCurlyLeft; _ -> Nothing

data TkBracketCurlyRight = TkBracketCurlyRight deriving (Eq, Ord, Show)
instance NodeData TkBracketCurlyRight
parseTkBracketCurlyRight :: Parser (NodeToken TkBracketCurlyRight)
parseTkBracketCurlyRight = genTokenParser Lexer.TkTBracketCurlyRight $ \case Lexer.TkBracketCurlyRight -> Just TkBracketCurlyRight; _ -> Nothing

data TkBracketRoundLeft = TkBracketRoundLeft deriving (Eq, Ord, Show)
instance NodeData TkBracketRoundLeft
parseTkBracketRoundLeft :: Parser (NodeToken TkBracketRoundLeft)
parseTkBracketRoundLeft = genTokenParser Lexer.TkTBracketRoundLeft $ \case Lexer.TkBracketRoundLeft -> Just TkBracketRoundLeft; _ -> Nothing

data TkBracketRoundRight = TkBracketRoundRight deriving (Eq, Ord, Show)
instance NodeData TkBracketRoundRight
parseTkBracketRoundRight :: Parser (NodeToken TkBracketRoundRight)
parseTkBracketRoundRight = genTokenParser Lexer.TkTBracketRoundRight$ \case Lexer.TkBracketRoundRight -> Just TkBracketRoundRight; _ -> Nothing

data TkColon = TkColon deriving (Eq, Ord, Show)
instance NodeData TkColon
parseTkColon :: Parser (NodeToken TkColon)
parseTkColon = genTokenParser Lexer.TkTColon $ \case Lexer.TkColon -> Just TkColon; _ -> Nothing

data TkComma = TkComma deriving (Eq, Ord, Show)
instance NodeData TkComma
parseTkComma :: Parser (NodeToken TkComma)
parseTkComma = genTokenParser Lexer.TkTComma$ \case Lexer.TkComma -> Just TkComma; _ -> Nothing

data TkDot = TkDot deriving (Eq, Ord, Show)
instance NodeData TkDot
parseTkDot :: Parser (NodeToken TkDot)
parseTkDot = genTokenParser Lexer.TkTDot$ \case Lexer.TkDot -> Just TkDot; _ -> Nothing

data TkEquals = TkEquals deriving (Eq, Ord, Show)
instance NodeData TkEquals
parseTkEquals :: Parser (NodeToken TkEquals)
parseTkEquals = genTokenParser Lexer.TkTEquals$ \case Lexer.TkEquals -> Just TkEquals; _ -> Nothing

newtype TkIdentifier = TkIdentifier Text deriving (Eq, Ord, Show)
instance NodeData TkIdentifier
parseTkIdentifier :: Parser (NodeToken TkIdentifier)
parseTkIdentifier = genTokenParser Lexer.TkTIdentifier$ \case Lexer.TkIdentifier txt -> Just $ TkIdentifier txt; _ -> Nothing

data TkSemicolon = TkSemicolon deriving (Eq, Ord, Show)
instance NodeData TkSemicolon
parseTkSemicolon :: Parser (NodeToken TkSemicolon)
parseTkSemicolon = genTokenParser Lexer.TkTSemicolon$ \case Lexer.TkSemicolon -> Just TkSemicolon; _ -> Nothing
{- ORMOLU_ENABLE -}

-- Complicated nodes
{- ORMOLU_DISABLE -}
data BlockContent = BlockContent [Node BlockStatementTerminated] (Node Expression) deriving (Eq, Ord, Show)
instance NodeData BlockContent
parseBlockContent :: Parser (Node BlockContent)
parseBlockContent = genCompoundParser $ BlockContent <$> genListParser parseBlockStatementTerminated <*> parseExpression

data BlockStatement
  = BlockStatementConstantDeclaration (NodeToken TkIdentifier) (NodeToken TkColon) (Node Expression)
  | BlockStatementConstantDefinition (NodeToken TkIdentifier) (NodeToken TkEquals) (Node Expression)
  deriving (Eq, Ord, Show)
instance NodeData BlockStatement
parseBlockStatement :: Parser (Node BlockStatement)
parseBlockStatement =
  msum
    [ genCompoundParser $ BlockStatementConstantDeclaration <$> parseTkIdentifier <*> parseTkColon <*> parseExpression,
      genCompoundParser $ BlockStatementConstantDefinition <$> parseTkIdentifier <*> parseTkEquals <*> parseExpression
    ]

data BlockStatementTerminated = BlockStatementTerminated (Node BlockStatement) (NodeToken TkSemicolon) deriving (Eq, Ord, Show)
instance NodeData BlockStatementTerminated
parseBlockStatementTerminated :: Parser (Node BlockStatementTerminated)
parseBlockStatementTerminated = genCompoundParser $ BlockStatementTerminated <$> parseBlockStatement <*> parseTkSemicolon

data Expression
  = ExpressionBlock (NodeToken TkBracketCurlyLeft) (Node BlockContent) (NodeToken TkBracketCurlyRight)
  | ExpressionFunctionCall (Node Expression) (NodeToken TkBracketRoundLeft) (Node Expression) (NodeToken TkBracketRoundRight)
  | ExpressionFunctionDefinition
      (NodeToken TkBracketRoundLeft)
      (Node FunctionDefinitionParameter)
      (NodeToken TkBracketRoundRight)
      (Maybe (NodeToken TkColon, Node Expression))
      (NodeToken TkBracketCurlyLeft)
      (Node BlockContent)
      (NodeToken TkBracketCurlyRight)
  | ExpressionFunctionType (NodeToken TkBracketRoundLeft) (Node Expression) (NodeToken TkBracketRoundRight) (NodeToken TkColon) (Node Expression)
  | ExpressionSymbolGlobal (Node GlobalIdentifier)
  | ExpressionSymbolLocal (Node LocalIdentifier)
  | ExpressionTypeDefinition (NodeToken TkBracketRoundLeft) [Node TypeDefinitionStatement] (NodeToken TkBracketRoundRight)
  | ExpressionTypeInstanciation (Node Expression) (NodeToken TkBracketRoundLeft) [Node TypeInstanciationStatement]  (NodeToken TkBracketRoundRight)
  deriving (Eq, Ord, Show)
instance NodeData Expression
parseExpression :: Parser (Node Expression)
parseExpression =
  addCompoundPostfixes
    ( msum
        [ genCompoundParser $ ExpressionBlock <$> parseTkBracketCurlyLeft <*> parseBlockContent <*> parseTkBracketCurlyRight,
          genCompoundParser $
            ExpressionFunctionDefinition
              <$> parseTkBracketRoundLeft
              <*> parseFunctionDefinitionParameter
              <*> parseTkBracketRoundRight
              <*> genMaybeParser ((,) <$> parseTkColon <*> parseExpression)
              <*> parseTkBracketCurlyLeft
              <*> parseBlockContent
              <*> parseTkBracketCurlyRight,
          genCompoundParser $
            ExpressionFunctionType
              <$> parseTkBracketRoundLeft
              <*> parseExpression
              <*> parseTkBracketRoundRight
              <*> parseTkColon
              <*> parseExpression,
          genCompoundParser $ ExpressionSymbolGlobal <$> parseGlobalIdentifier,
          genCompoundParser $ ExpressionSymbolLocal <$> parseLocalIdentifier,
          genCompoundParser $
            ExpressionTypeDefinition
              <$> parseTkBracketRoundLeft
              <*> genListSeparatedParser parseTypeDefinitionStatement parseTkComma
              <*> parseTkBracketRoundRight
        ]
    )
    [ \f -> ExpressionFunctionCall f <$> parseTkBracketRoundLeft <*> parseExpression <*> parseTkBracketRoundRight,
      \f ->
        ExpressionTypeInstanciation f
          <$> parseTkBracketRoundLeft
          <*> genListSeparatedParser parseTypeInstanciationStatement parseTkComma
          <*> parseTkBracketRoundRight
    ]

data FunctionDefinitionParameter = FunctionDefinitionParameter (Node LocalIdentifier) (NodeToken TkColon) (Node Expression)
  deriving (Eq, Ord, Show)
instance NodeData FunctionDefinitionParameter
parseFunctionDefinitionParameter :: Parser (Node FunctionDefinitionParameter)
parseFunctionDefinitionParameter = genCompoundParser $ FunctionDefinitionParameter <$> parseLocalIdentifier <*> parseTkColon <*> parseExpression

data GlobalIdentifier = GlobalIdentifier (Node NamespaceReference) (NodeToken TkIdentifier)
  deriving (Eq, Ord, Show)
instance NodeData GlobalIdentifier
parseGlobalIdentifier :: Parser (Node GlobalIdentifier)
parseGlobalIdentifier = genCompoundParser $ GlobalIdentifier <$> parseNamespaceReference <*> parseTkIdentifier

newtype LocalIdentifier = LocalIdentifier (NodeToken TkIdentifier)
  deriving (Eq, Ord, Show)
instance NodeData LocalIdentifier
parseLocalIdentifier :: Parser (Node LocalIdentifier)
parseLocalIdentifier = genCompoundParser $ LocalIdentifier <$> parseTkIdentifier


data NamespaceReference
  = NamespaceReferenceGlobal (NodeToken TkIdentifier) (NodeToken TkDot) (Maybe (Node NamespaceReferenceSubnamespace))
  | NamespaceReferenceProject (NodeToken TkDot) (Maybe (Node NamespaceReferenceSubnamespace))
  deriving (Eq, Ord, Show)
instance NodeData NamespaceReference
parseNamespaceReference :: Parser (Node NamespaceReference)
parseNamespaceReference =
  msum
    [ genCompoundParser $ NamespaceReferenceGlobal <$> parseTkIdentifier <*> parseTkDot <*> genMaybeParser parseNamespaceReferenceSubnamespace,
      genCompoundParser $ NamespaceReferenceProject <$> parseTkDot <*> genMaybeParser parseNamespaceReferenceSubnamespace
    ]

data NamespaceReferenceSubnamespace = NamespaceReferenceSubnamespace (NodeToken TkIdentifier) (NodeToken TkDot) (Maybe (Node NamespaceReferenceSubnamespace))
  deriving (Eq, Ord, Show)
instance NodeData NamespaceReferenceSubnamespace
parseNamespaceReferenceSubnamespace :: Parser (Node NamespaceReferenceSubnamespace)
parseNamespaceReferenceSubnamespace =
  genCompoundParser $
    NamespaceReferenceSubnamespace
      <$> parseTkIdentifier
      <*> parseTkDot
      <*> genMaybeParser parseNamespaceReferenceSubnamespace

data TypeDefinitionStatement = TypeDefinitionStatementProperty (Node GlobalIdentifier) (NodeToken TkColon) (Node Expression)
  deriving (Eq, Ord, Show)
instance NodeData TypeDefinitionStatement
parseTypeDefinitionStatement :: Parser (Node TypeDefinitionStatement)
parseTypeDefinitionStatement = genCompoundParser $ TypeDefinitionStatementProperty <$> parseGlobalIdentifier <*> parseTkColon <*> parseExpression

data TypeInstanciationStatement = TypeInstanciationStatementProperty (Node GlobalIdentifier) (NodeToken TkEquals) (Node Expression)
  deriving (Eq, Ord, Show)
instance NodeData TypeInstanciationStatement
parseTypeInstanciationStatement :: Parser (Node TypeInstanciationStatement)
parseTypeInstanciationStatement = genCompoundParser $ TypeInstanciationStatementProperty <$> parseGlobalIdentifier <*> parseTkEquals <*> parseExpression
{- ORMOLU_ENABLE -}

-- Parsing a full file
type File = General.File Tokenizer.Error Lexer.TokenType (Node BlockContent)

parse :: Lexer.File -> File
parse = General.runParser parseBlockContent
