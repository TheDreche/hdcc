{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections #-}

module Hdcc.Dml.Link
  ( Error (..),
    File (File),
    GlobalIdentifier (GlobalIdentifier),
    Linked,
    LinkedT,
    SymbolConstant (SymbolConstant, scDeclarations, scDefinitions),
    SymbolParameter (SymbolParameter, spDeclaration),
    Value (..),
    ValuePositioned (ValuePositioned),
    convertFile,
    genIOLoader,
    symbolDeclarations,
  )
where

import Control.Arrow (Arrow (second))
import Control.Exception (handle, try)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Control.Monad.Trans.Accum (Accum, add, evalAccum, look)
import Control.Monad.Trans.Except (Except, throwE)
import Control.Monad.Trans.Reader (Reader, ask, runReader, withReaderT)
import Data.Function (fix)
import Data.Functor ((<&>))
import Data.Functor.Identity (Identity ())
import Data.Map (Map, fromList)
import qualified Data.Map as Map
import Data.Maybe (catMaybes, fromJust, mapMaybe, maybeToList)
import Data.Monoid (Sum (Sum))
import qualified Data.Set as Set
import Data.Text (Text, unpack)
import Data.Text.IO (readFile)
import qualified Hdcc.Dml.Lexer as Lexer
import qualified Hdcc.Dml.Parser as Parser
import qualified Hdcc.Dml.Tokenizer as Tokenizer
import qualified Hdcc.General.Error as Error
import qualified Hdcc.General.Link as General
import Hdcc.General.Parser (Correction, Node (Node), NodeToken (NodeToken))
import qualified Hdcc.General.Parser
import Hdcc.General.Position (Span)
import qualified Hdcc.General.Set as S
import System.FilePath ((</>))
import System.IO.Error (isDoesNotExistError)
import Prelude hiding (error, exp, lookup, read, readFile)

data Error = EIO GlobalIdentifier FilePath IOError deriving (Eq, Show)

newtype GlobalIdentifier = GlobalIdentifier [Text] deriving (Eq, Ord, Show)

-- Symbols
data SymbolConstant = SymbolConstant
  { scNumber :: Integer,
    scDeclarations :: Set.Set (Span, ValuePositioned),
    scDefinitions :: Set.Set (Span, ValuePositioned)
  }
  deriving (Eq, Ord, Show)

data SymbolParameter = SymbolParameter {spNumber :: Integer, spDeclaration :: (Span, ValuePositioned)}
  deriving (Eq, Ord, Show)

symbolDeclarations :: Either SymbolConstant SymbolParameter -> Set.Set (Span, ValuePositioned)
symbolDeclarations = either scDeclarations $ Set.singleton . spDeclaration

newtype File = File (S.Set (Except Error (Set.Set ([Correction Lexer.TokenType], ValuePositioned), [Error.Error Tokenizer.Error]))) deriving (Eq, Show)

data Value
  = VGlobal GlobalIdentifier
  | VBlock (Map Text SymbolConstant) ValuePositioned
  | VFunctionCall ValuePositioned ValuePositioned
  | VFunctionDefinition (Maybe Text) SymbolParameter ValuePositioned (Maybe ValuePositioned) -- Parameter, result, result type
  | VFunctionType ValuePositioned ValuePositioned -- Parameter, result type
  | VTypeDefinition (Map GlobalIdentifier ValuePositioned)
  | VTypeInstanciation ValuePositioned (Map GlobalIdentifier ValuePositioned)
  | VLocal Text (Either SymbolConstant SymbolParameter)
  | VLocalUndeclared (NodeToken Parser.TkIdentifier)
  deriving (Eq, Ord, Show)

data ValuePositioned = ValuePositioned Span Value deriving (Eq, Ord, Show)

-- Linked monad
type LinkedT l = General.LinkedT GlobalIdentifier l File

type Linked l = LinkedT l Identity

-- Loader for external files
type LoaderImplementation = General.LoaderImplementation GlobalIdentifier File

-- Convert Parser.File to Linker
type LinkerScoped t = Accum (Sum Integer) (Reader (Map Text (Either SymbolConstant SymbolParameter)) t)

convertFile :: Node Parser.BlockContent -> ValuePositioned
convertFile file =
  (`runReader` Map.empty) . (`evalAccum` Sum 0) $
    let unTkIdentifier (Parser.TkIdentifier name) = name
        convertNode :: (a -> LinkerScoped Value) -> Node a -> LinkerScoped ValuePositioned
        convertNode f (Node p i) = fmap (ValuePositioned p) <$> f i
        convertGlobalIdentifier :: Parser.GlobalIdentifier -> GlobalIdentifier
        convertGlobalIdentifier (Parser.GlobalIdentifier (Node _ namespace_reference) (NodeToken _ name)) =
          GlobalIdentifier $ convertNamespaceReference namespace_reference ++ maybeToList (name <&> unTkIdentifier)
        convertExpression :: Node Parser.Expression -> LinkerScoped ValuePositioned
        convertExpression =
          convertNode
            ( \case
                Parser.ExpressionBlock _ (Node _ block_content) _ -> fmap (uncurry VBlock) <$> convertBlockContent block_content
                Parser.ExpressionFunctionCall function _ parameter _ ->
                  liftA2 VFunctionCall <$> convertExpression function <*> convertExpression parameter
                ( Parser.ExpressionFunctionDefinition
                    _
                    ( Node
                        parameter_definition_position
                        ( Parser.FunctionDefinitionParameter
                            (Node _ (Parser.LocalIdentifier (NodeToken _ parameter_identifier)))
                            _
                            parameter_type
                          )
                      )
                    _
                    return_type_annotation
                    _
                    block_node
                    _
                  ) -> do
                    let parameter_name = parameter_identifier <&> unTkIdentifier
                    (Sum parameter_number) <- look <* add (Sum 1) -- TODO: Add number to parameter
                    parameter_type' <- convertExpression parameter_type
                    let parameter_symbol = SymbolParameter parameter_number . (parameter_definition_position,) <$> parameter_type'
                    result_and_type <-
                      fmap (withReaderT (\parent_scope -> maybe id (`Map.insert` Right (runReader parameter_symbol parent_scope)) parameter_name parent_scope)) $
                        liftA2 (,)
                          <$> convertNode (fmap (fmap $ uncurry VBlock) . convertBlockContent) block_node
                          <*> maybe (pure $ pure Nothing) (fmap (fmap Just) . convertExpression . snd) return_type_annotation
                    return $ uncurry . VFunctionDefinition parameter_name <$> parameter_symbol <*> result_and_type
                Parser.ExpressionFunctionType _ parameter_type _ _ result_type ->
                  liftA2 VFunctionType <$> convertExpression parameter_type <*> convertExpression result_type
                Parser.ExpressionSymbolGlobal (Node _ identifier) ->
                  return $ return $ VGlobal $ convertGlobalIdentifier identifier
                Parser.ExpressionSymbolLocal (Node _ (Parser.LocalIdentifier identifier@(NodeToken _ name))) ->
                  case name of
                    Nothing -> return $ return $ VLocalUndeclared identifier
                    Just (Parser.TkIdentifier name') -> return $ ask <&> maybe (VLocalUndeclared identifier) (VLocal name') . Map.lookup name'
                Parser.ExpressionTypeDefinition _ type_definition_statements _ ->
                  fmap (VTypeDefinition . fromList) . sequence
                    <$> mapM
                      ( \(Node _ (Parser.TypeDefinitionStatementProperty (Node _ lhs) _ rhs)) ->
                          fmap (convertGlobalIdentifier lhs,) <$> convertExpression rhs
                      )
                      type_definition_statements
                Parser.ExpressionTypeInstanciation type_ _ type_instanciation_statements _ ->
                  liftA2 VTypeInstanciation
                    <$> convertExpression type_
                    <*> fmap
                      (fmap fromList . sequence)
                      ( mapM
                          ( \(Node _ (Parser.TypeInstanciationStatementProperty (Node _ lhs) _ rhs)) ->
                              fmap (convertGlobalIdentifier lhs,) <$> convertExpression rhs
                          )
                          type_instanciation_statements
                      )
            )
        convertNamespaceReference :: Parser.NamespaceReference -> [Text]
        convertNamespaceReference =
          let convertSubnamespace (Just (Node _ (Parser.NamespaceReferenceSubnamespace (NodeToken _ identifier) _ next))) =
                maybeToList (unTkIdentifier <$> identifier) ++ convertSubnamespace next
              convertSubnamespace Nothing = []
           in \case
                (Parser.NamespaceReferenceGlobal (NodeToken _ global_identifier) _ subnamespace) -> maybeToList (unTkIdentifier <$> global_identifier) ++ convertSubnamespace subnamespace
                (Parser.NamespaceReferenceProject _ subnamespace) -> convertSubnamespace subnamespace
        convertBlockContent :: Parser.BlockContent -> LinkerScoped (Map Text SymbolConstant, ValuePositioned)
        convertBlockContent (Parser.BlockContent statements expression) =
          let locals :: Map Text (LinkerScoped (Set.Set (Span, ValuePositioned), Set.Set (Span, ValuePositioned)))
              locals =
                Map.fromListWith
                  (liftA2 $ liftA2 $ \(l_decls, l_defs) (r_decls, r_defs) -> (Set.union l_decls r_decls, Set.union l_defs r_defs))
                  $ mapMaybe
                    ( \(Node position (Parser.BlockStatementTerminated (Node _ statement) _)) -> case statement of
                        Parser.BlockStatementConstantDefinition (NodeToken _ identifier) _ value ->
                          identifier <&> (,convertExpression value <&> fmap ((Set.empty,) . Set.singleton . (position,))) . unTkIdentifier
                        Parser.BlockStatementConstantDeclaration (NodeToken _ identifier) _ type_ ->
                          identifier <&> (,convertExpression type_ <&> fmap ((,Set.empty) . Set.singleton . (position,))) . unTkIdentifier
                    )
                    statements
              locals' :: Accum (Sum Integer) (Map Text (LinkerScoped SymbolConstant))
              locals' = mapM (\item -> (look <* add (Sum 1)) <&> \(Sum parameter_number) -> fmap (uncurry $ SymbolConstant parameter_number) <$> item) locals -- TODO: Assign file identifier
              locals'' :: LinkerScoped (Map Text SymbolConstant)
              locals'' = fmap sequence $ locals' >>= sequence
           in ( \generate_scope result_expression -> do
                  parent_scope <- ask
                  let scope = fix $ \scope' -> Map.union (Left <$> runReader generate_scope scope') parent_scope
                  pure (runReader generate_scope scope, runReader result_expression scope)
              )
                <$> locals''
                <*> convertExpression expression
     in convertNode (fmap (fmap $ uncurry VBlock) . convertBlockContent) file

parserFile2linkFile :: S.Set (Except Error Parser.File) -> File
parserFile2linkFile = File . fmap (<&> \(Hdcc.General.Parser.File parses errors) -> (second convertFile `Set.map` parses, errors))

-- Generate an IO project
genIOLoader :: (MonadIO m) => Map FilePath (Set.Set FilePath) -> LoaderImplementation m (Set.Set FilePath, GlobalIdentifier)
genIOLoader deps =
  let globalIdentifier2FilePath :: GlobalIdentifier -> FilePath -> FilePath
      globalIdentifier2FilePath (GlobalIdentifier subpath_components_text) root = (++ ".dml") $ foldl (</>) root $ unpack <$> subpath_components_text
      loadFileContent :: Except Error Text -> Except Error Parser.File
      loadFileContent = fmap (Parser.parse . Lexer.lex . Tokenizer.tokenize)
      loadFile :: (MonadIO m) => GlobalIdentifier -> FilePath -> m (Maybe (Except Error Parser.File))
      loadFile dep file_path =
        liftIO
          $ fmap (fmap loadFileContent)
          $ handle
            ( \e ->
                if isDoesNotExistError e
                  then return Nothing
                  else return $ Just $ throwE $ EIO dep file_path e
            )
          $ Just . return <$> readFile file_path
   in General.LoaderImplementation
        ( \(roots, identifier) ->
            fmap (parserFile2linkFile . S.fromList . catMaybes) $
              mapM (loadFile identifier) $
                Set.elems $
                  Set.map (globalIdentifier2FilePath identifier) roots
        )
        ( \(roots, _) name_global ->
            fmap ((,name_global) . Set.fromList . catMaybes)
              $ mapM
                ( \root ->
                    liftIO
                      $ fmap
                        ( \case
                            Left e -> if isDoesNotExistError e then Nothing else Just root
                            Right _ -> Just root
                        )
                      $ try
                      $ readFile
                      $ globalIdentifier2FilePath name_global root
                )
              $ Set.toList
              $ foldr Set.union Set.empty
              $ Set.map (\root -> fromJust $ Map.lookup root deps) roots
        )
