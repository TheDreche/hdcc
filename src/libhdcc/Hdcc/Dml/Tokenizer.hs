{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}

module Hdcc.Dml.Tokenizer
  ( Error (EUnterminatedMultilineComment, ENoNewlineAtEof),
    File,
    Token (..),
    tokenize,
  )
where

import Data.Text (Text)
import qualified Data.Text as Text
import Data.Text.ICU.Char (Bool_ (WhiteSpace, XidContinue), property)
import Hdcc.General.Error (ErrorData)
import Hdcc.General.Tokenizer (genTokenizer)
import qualified Hdcc.General.Tokenizer as Tokenizer

data Token
  = TkBracketCurlyLeft
  | TkBracketCurlyRight
  | TkBracketRoundLeft
  | TkBracketRoundRight
  | TkColon
  | TkComma
  | TkCommentEol -- TODO: Warning (EOF)
  | TkCommentMultiline (Maybe (Text, Text)) -- TODO: Data
  | TkDot
  | TkEquals
  | TkIdentifier Text
  | TkSemicolon
  | TkWhitespace
  deriving (Eq, Show)

data Error = EUnterminatedMultilineComment | ENoNewlineAtEof deriving (Eq, Ord, Show)

instance ErrorData Error

instance Tokenizer.TokenData Token

type File = Tokenizer.File Error Token

tokenize :: Text -> File
tokenize = genTokenizer tokenizers
  where
    specialCharToken :: (Text, Token) -> Text -> Maybe (Token, Integer, [Error])
    specialCharToken (token_text, token_type) source_remaining =
      if Text.isPrefixOf token_text source_remaining
        then Just (token_type, toInteger $ Text.length token_text, [])
        else Nothing
    specialCharTokens :: [Text -> Maybe (Token, Integer, [Error])]
    specialCharTokens =
      specialCharToken
        <$> [ ("(", TkBracketRoundLeft),
              (")", TkBracketRoundRight),
              ("{", TkBracketCurlyLeft),
              ("}", TkBracketCurlyRight),
              (":", TkColon),
              (",", TkComma),
              (".", TkDot),
              ("=", TkEquals),
              (";", TkSemicolon)
            ]
    identifierToken :: Text -> Maybe (Token, Integer, [Error])
    identifierToken source_remaining =
      let content = Text.takeWhile (property XidContinue) source_remaining
       in if Text.null content then Nothing else Just (TkIdentifier content, toInteger $ Text.length content, [])
    whitespaceToken :: Text -> Maybe (Token, Integer, [Error])
    whitespaceToken source_remaining =
      if property WhiteSpace $ Text.head source_remaining
        then Just (TkWhitespace, toInteger $ Text.length $ Text.takeWhile (property WhiteSpace) source_remaining, [])
        else Nothing
    commentEolToken :: Text -> Maybe (Token, Integer, [Error])
    commentEolToken source_remaining =
      if Text.isPrefixOf "//" source_remaining
        then Just (TkCommentEol, toInteger $ Text.length $ Text.takeWhile (/= '\n') source_remaining, [])
        else Nothing
    commentMultilineToken :: Text -> Maybe (Token, Integer, [Error])
    commentMultilineToken source_remaining =
      if Text.isPrefixOf "/*" source_remaining
        then
          let b = Text.breakOn "*/" source_remaining
           in Just
                ( TkCommentMultiline Nothing,
                  (2 +) $ toInteger $ Text.length $ fst b,
                  case snd b of
                    "" -> [EUnterminatedMultilineComment]
                    _ -> []
                )
        else Nothing
    tokenizers = specialCharTokens ++ [identifierToken, whitespaceToken, commentEolToken, commentMultilineToken]
