{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE MultiParamTypeClasses #-}

module Hdcc.Dml.Lexer (File, Token (..), TokenType (..), lex) where

import Data.Text (Text)
import qualified Hdcc.Dml.Tokenizer as Tokenizer
import Hdcc.General.Lexer (advance, genLexer, getCurrentSpan, remainder)
import qualified Hdcc.General.Lexer as Lexer
import Prelude hiding (lex)

data Token
  = TkBracketCurlyLeft
  | TkBracketCurlyRight
  | TkBracketRoundLeft
  | TkBracketRoundRight
  | TkColon
  | TkComma
  | TkDot
  | TkEquals
  | TkIdentifier Text
  | TkSemicolon
  deriving (Eq, Ord, Show)

data TokenType
  = TkTBracketCurlyLeft
  | TkTBracketCurlyRight
  | TkTBracketRoundLeft
  | TkTBracketRoundRight
  | TkTColon
  | TkTComma
  | TkTDot
  | TkTEquals
  | TkTIdentifier
  | TkTSemicolon
  deriving (Eq, Ord, Show)

instance Lexer.TokenData TokenType Token where
  tokenType TkBracketCurlyLeft = TkTBracketCurlyLeft
  tokenType TkBracketCurlyRight = TkTBracketCurlyRight
  tokenType TkBracketRoundLeft = TkTBracketRoundLeft
  tokenType TkBracketRoundRight = TkTBracketRoundRight
  tokenType TkColon = TkTColon
  tokenType TkComma = TkTComma
  tokenType TkDot = TkTDot
  tokenType TkEquals = TkTEquals
  tokenType (TkIdentifier _) = TkTIdentifier
  tokenType TkSemicolon = TkTSemicolon

type File = Lexer.File Tokenizer.Error TokenType Token

lex :: Tokenizer.File -> File
lex = genLexer $ do
  remaining <- remainder
  position <- getCurrentSpan
  advance 1
  let token a = return [Lexer.Token position $ Right a]
  case remaining of
    Tokenizer.TkBracketCurlyLeft : _ -> token TkBracketCurlyLeft
    Tokenizer.TkBracketCurlyRight : _ -> token TkBracketCurlyRight
    Tokenizer.TkBracketRoundLeft : _ -> token TkBracketRoundLeft
    Tokenizer.TkBracketRoundRight : _ -> token TkBracketRoundRight
    Tokenizer.TkColon : _ -> token TkColon
    Tokenizer.TkComma : _ -> token TkComma
    Tokenizer.TkCommentEol : _ -> return []
    (Tokenizer.TkCommentMultiline _) : _ -> return []
    Tokenizer.TkDot : _ -> token TkDot
    Tokenizer.TkEquals : _ -> token TkEquals
    (Tokenizer.TkIdentifier name) : _ -> token $ TkIdentifier name
    Tokenizer.TkSemicolon : _ -> token TkSemicolon
    Tokenizer.TkWhitespace : _ -> return []
    [] -> error "Called on empty list"
