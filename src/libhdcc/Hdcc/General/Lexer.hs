{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}

module Hdcc.General.Lexer
  ( File (File),
    Lexer (Lexer),
    Token (Token),
    TokenData (tokenType),
    advance,
    genLexer,
    getCurrentSpan,
    remainder,
  )
where

import Control.Monad.Trans.State (State, get, modify, runState)
import Hdcc.General.Error (Error)
import Hdcc.General.Position (Span)
import qualified Hdcc.General.Tokenizer as Tokenizer

-- There mustn't be an error that can happen during the lexing stage (which essentially just discards useless tokens like comments), they should have happened during the tolenizer instead.

class (Eq token_data, Ord token_data, Show token_data, Eq token_type, Ord token_type, Show token_type) => TokenData token_type token_data where
  tokenType :: token_data -> token_type

data Token tt t where
  Token :: (TokenData tt t) => Span -> Either String t -> Token tt t

data File e tt t = File Span [Token tt t] [Error e] deriving (Eq, Ord, Show)

deriving instance Eq (Token tt t)

deriving instance Show (Token tt t)

instance (TokenData tt t) => Ord (Token tt t) where
  compare (Token a _) (Token b _) = compare a b

genLexer :: (TokenData ltt lt) => Lexer eo tt [Token ltt lt] -> Tokenizer.File eo tt -> File eo ltt lt
genLexer (Lexer lexer) (Tokenizer.File source_span tokens errors) =
  let genLexerRecursive [] = []
      genLexerRecursive remaining_tokens = case runState lexer $ LexerState remaining_tokens of
        (result, LexerState remaining) -> result ++ stripErrors remaining
      stripErrors (Tokenizer.Token position (Left content) : remaining) = Token position (Left content) : stripErrors remaining
      stripErrors remaining = genLexerRecursive remaining
   in File source_span (stripErrors tokens) errors

-- Lexer state
data LexerState eo t where
  LexerState ::
    [Tokenizer.Token t] ->
    LexerState eo t

-- Lexer monad
newtype Lexer eo t v = Lexer (State (LexerState eo t) v)

instance (Tokenizer.TokenData t) => Functor (Lexer eo t) where
  fmap f (Lexer l) = Lexer $ fmap f l

instance (Tokenizer.TokenData t) => Applicative (Lexer eo t) where
  pure = Lexer . pure
  Lexer a <*> Lexer b = Lexer $ a <*> b

instance (Tokenizer.TokenData t) => Monad (Lexer eo t) where
  Lexer a >>= b = Lexer $ a >>= (\case Lexer b' -> b') . b

advance :: Integer -> Lexer eo t ()
advance number = Lexer $ modify (\(LexerState remaining) -> LexerState $ drop (fromInteger number) remaining)

remainder :: Lexer eo t [t]
remainder =
  Lexer $
    ( \(LexerState remaining) ->
        foldr
          ( \(Tokenizer.Token _ t) remaining' -> case t of
              Left _ -> []
              Right v -> v : remaining'
          )
          []
          remaining
    )
      <$> get

getCurrentSpan :: Lexer eo t Span
getCurrentSpan = Lexer $ (\(LexerState remaining) -> case head remaining of Tokenizer.Token s _ -> s) <$> get
