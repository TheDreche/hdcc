{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Hdcc.General.Dependent (Dependent (type2value), List (ListEmpty, ListPrepend), Peano (PeanoZero, PeanoSucc)) where

import qualified Data.Peano as P

-- Class
class Dependent t w where
  type2value :: w (v :: t) -> t

instance Dependent () f where
  type2value _ = ()

-- Dependent list
data List t (l :: [t]) where
  ListEmpty :: List t '[]
  ListPrepend :: (Dependent t w) => List t content -> w (v :: t) -> List t (v : content)

instance Dependent [t] (List t) where
  type2value ListEmpty = []
  type2value (ListPrepend prev leading) = type2value leading : type2value prev

-- Dependent peano
data Peano (p :: P.Peano) where
  PeanoZero :: Peano P.Zero
  PeanoSucc :: Peano v -> Peano (P.Succ v)

instance Dependent P.Peano Peano where
  type2value PeanoZero = P.Zero
  type2value (PeanoSucc v) = P.Succ $ type2value v
