{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}

module Hdcc.General.Error (ErrorData, Error (Error)) where

import Hdcc.General.Position (Span)

class (Eq e, Ord e, Show e) => ErrorData e

data Error e where
  Error :: (ErrorData e) => Span -> e -> Error e

deriving instance Eq (Error e)

deriving instance Ord (Error e)

deriving instance Show (Error e)
