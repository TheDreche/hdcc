{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}

module Hdcc.General.Set
  ( SetT,
    Set,
    cartesianProduct,
    delete,
    deleteA,
    difference,
    disjoint,
    disjointT,
    disjointUnion,
    empty,
    filter,
    filterM,
    fromList,
    fromSet,
    fromSetT,
    fromTransformer,
    insert,
    insertT,
    intersection,
    isProperSubsetOf,
    isProperSubsetOfT,
    isSubsetOf,
    isSubsetOfT,
    mapSetT,
    member,
    memberT,
    notMember,
    notMemberT,
    null,
    nullT,
    optimize,
    partition,
    partitionM,
    partitionMT,
    partitionT,
    singleton,
    singletonT,
    size,
    sizeT,
    toSet,
    toSetT,
    toTransformer,
    toTransformerI,
    toTransformerIM,
    union,
    singleT,
    single,
  )
where

import Control.Applicative (Alternative (empty, (<|>)))
import Control.Monad (MonadPlus, join)
import Control.Monad.Trans.Class (MonadTrans (lift))
import Data.Bifunctor (Bifunctor (bimap))
import Data.Functor ((<&>))
import Data.Functor.Identity (Identity (Identity, runIdentity))
import qualified Data.List as List
import Data.Monoid.Cancellative (GCDMonoid)
import Data.Monoid.Factorial (FactorialMonoid)
import qualified Data.Monoid.Factorial as M
import Data.Monoid.GCD
  ( DistributiveGCDMonoid,
    GCDMonoid (gcd),
    LeftDistributiveGCDMonoid,
    LeftGCDMonoid (commonPrefix),
    OverlappingGCDMonoid (stripOverlap),
    RightDistributiveGCDMonoid,
    RightGCDMonoid (commonSuffix),
  )
import Data.Monoid.LCM (DistributiveLCMMonoid, LCMMonoid (lcm))
import Data.Monoid.Monus (Monus ((<\>)))
import Data.Monoid.Null (MonoidNull (null))
import Data.Semigroup.Cancellative (Commutative, LeftReductive (stripPrefix), Reductive ((</>)), RightReductive (stripSuffix))
import Data.Semigroup.Factorial (Factorial)
import qualified Data.Set as Set
import Prelude hiding (filter, null)
import qualified Prelude

newtype SetT m i = SetT (m [i])

type Set = SetT Identity

instance (Eq k) => Eq (Set k) where
  lhs == rhs = isSubsetOf lhs rhs && isSubsetOf rhs lhs

deriving instance (Read k) => Read (Set k)

deriving instance (Show k) => Show (Set k)

instance Foldable Set where
  foldMap f (SetT (Identity s)) = foldMap f s

instance Traversable Set where
  traverse f (SetT (Identity s)) = SetT . Identity <$> traverse f s

instance (Applicative m) => Alternative (SetT m) where
  empty = SetT $ pure []
  (<|>) = union

instance (Applicative m) => Semigroup (SetT m v) where
  (<>) = union

instance (Applicative m) => Monoid (SetT m v) where
  mempty = empty

instance MonoidNull (Set v) where
  null (SetT (Identity c)) = Prelude.null c

instance (Eq v) => Factorial (Set v) where
  factors (SetT (Identity c)) = List.nub c <&> SetT . Identity . List.singleton

instance (Eq v) => FactorialMonoid (Set v)

instance Commutative (Set v)

instance (Eq v) => LeftReductive (Set v) where
  stripPrefix pre cont = if isSubsetOf pre cont then Just $ difference cont pre else Nothing

instance (Eq v) => RightReductive (Set v) where
  stripSuffix = stripPrefix

instance (Eq v) => Reductive (Set v) where
  lhs </> rhs = if rhs `isSubsetOf` lhs then Just $ difference lhs rhs else Nothing

instance (Eq v) => LeftGCDMonoid (Set v) where
  commonPrefix = intersection

instance (Eq v) => RightGCDMonoid (Set v) where
  commonSuffix = intersection

instance (Eq v) => OverlappingGCDMonoid (Set v) where
  stripOverlap lhs rhs = (difference lhs rhs, intersection lhs rhs, difference rhs lhs)

instance (Eq v) => LeftDistributiveGCDMonoid (Set v)

instance (Eq v) => RightDistributiveGCDMonoid (Set v)

instance (Eq v) => DistributiveGCDMonoid (Set v)

instance (Eq v) => GCDMonoid (Set v) where
  gcd = intersection

instance (Eq v) => LCMMonoid (Set v) where
  lcm = union

instance (Eq v) => DistributiveLCMMonoid (Set v)

instance (Eq v) => Monus (Set v) where
  (<\>) = difference

deriving instance (Functor m) => Functor (SetT m)

instance (Applicative m) => Applicative (SetT m) where
  pure = SetT . pure . (: [])
  SetT lhs <*> SetT rhs = SetT $ (<*>) <$> lhs <*> rhs

instance (Monad m) => Monad (SetT m) where
  SetT lhs >>= rhs = SetT $ do
    lhs' <- lhs
    join <$> mapM ((\(SetT v) -> v) . rhs) lhs'

instance MonadTrans SetT where
  lift a = SetT $ pure <$> a

instance (Monad m) => MonadFail (SetT m) where
  fail _ = empty

instance (Monad m) => MonadPlus (SetT m)

toSet :: (Ord k) => Set k -> Set.Set k
toSet (SetT set) = Set.fromList $ runIdentity set

toSetT :: (Ord k, Functor m) => SetT m k -> m (Set.Set k)
toSetT (SetT set) = Set.fromList <$> set

fromSet :: Set.Set k -> Set k
fromSet = SetT . Identity . Set.toList

fromSetT :: (Functor m) => m (Set.Set k) -> SetT m k
fromSetT m = SetT $ m <&> Set.toList

fromTransformer :: (Functor m) => SetT m k -> m (Set k)
fromTransformer (SetT v) = v <&> SetT . Identity

toTransformer :: (Functor m) => m (Set k) -> SetT m k
toTransformer v = SetT $ v <&> (\(SetT (Identity v')) -> v')

toTransformerI :: (Applicative m) => Set (m k) -> SetT m k
toTransformerI (SetT (Identity v)) = SetT $ sequenceA v

toTransformerIM :: (Monad m) => Set (m k) -> SetT m k
toTransformerIM (SetT (Identity v)) = SetT $ sequence v

optimize :: (Eq k, Functor m) => SetT m k -> SetT m k
optimize (SetT l) = SetT $ l <&> List.nub

singleton :: k -> Set k
singleton = singletonT . Identity

singletonT :: (Functor m) => m k -> SetT m k
singletonT v = fromListT $ v <&> (: [])

single :: (Eq k) => (k -> r) -> r -> Set k -> r
single t f = runIdentity . singleT (Identity . t) (Identity f)

singleT :: (Monad m, Eq k) => (k -> m r) -> m r -> SetT m k -> m r
singleT t f (SetT s) = s >>= (\case [v] -> t v; _ -> f) . List.nub

fromList :: [k] -> Set k
fromList = fromListT . Identity

fromListT :: m [k] -> SetT m k
fromListT = SetT

insert :: k -> Set k -> Set k
insert = insertT . Identity

insertT :: (Monad m) => m k -> SetT m k -> SetT m k
insertT item (SetT set) = SetT $ do
  set' <- set
  (: set') <$> item

delete :: (Eq k, Functor m) => k -> SetT m k -> SetT m k
delete item (SetT set) = SetT $ Prelude.filter (/= item) <$> set

deleteA :: (Eq k, Applicative m) => m k -> SetT m k -> SetT m k
deleteA item (SetT set) = SetT $ (\set' item' -> Prelude.filter (/= item') set') <$> set <*> item

mapSetT :: (m [a] -> n [b]) -> SetT m a -> SetT n b
mapSetT f (SetT s) = SetT $ f s

member :: (Eq k) => k -> Set k -> Bool
member item = runIdentity . memberT item

memberT :: (Eq k, Functor m) => k -> SetT m k -> m Bool
memberT item (SetT set) = elem item <$> set

notMember :: (Eq k) => k -> Set k -> Bool
notMember item = runIdentity . notMemberT item

notMemberT :: (Eq k, Functor m) => k -> SetT m k -> m Bool
notMemberT item set = not <$> memberT item set

nullT :: (Functor m) => SetT m k -> m Bool
nullT (SetT set) = Prelude.null <$> set

size :: (Eq k) => Set k -> Integer
size = runIdentity . sizeT

sizeT :: (Eq k, Functor m) => SetT m k -> m Integer
sizeT (SetT set) = set <&> toInteger . length . List.nub

isSubsetOf :: (Eq k) => Set k -> Set k -> Bool
isSubsetOf lhs = runIdentity . isSubsetOfT lhs

isSubsetOfT :: (Eq k, Applicative m) => SetT m k -> SetT m k -> m Bool
isSubsetOfT (SetT lhs) (SetT rhs) = (\lhs' rhs' -> all (`elem` rhs') lhs') <$> lhs <*> rhs

isProperSubsetOf :: (Eq k) => Set k -> Set k -> Bool
isProperSubsetOf lhs = runIdentity . isProperSubsetOfT lhs

isProperSubsetOfT :: (Eq k, Applicative m) => SetT m k -> SetT m k -> m Bool
isProperSubsetOfT (SetT lhs) (SetT rhs) = (\lhs' rhs' -> all (`elem` rhs') lhs' && any (`notElem` lhs') rhs') <$> lhs <*> rhs

disjoint :: (Eq k) => Set k -> Set k -> Bool
disjoint lhs = runIdentity . disjointT lhs

disjointT :: (Eq k, Applicative m) => SetT m k -> SetT m k -> m Bool
disjointT (SetT lhs) (SetT rhs) = (\lhs' rhs' -> not $ any (`elem` rhs') lhs') <$> lhs <*> rhs

union :: (Applicative m) => SetT m k -> SetT m k -> SetT m k
union (SetT lhs) (SetT rhs) = SetT $ (++) <$> lhs <*> rhs

difference :: (Eq k, Applicative m) => SetT m k -> SetT m k -> SetT m k
difference (SetT lhs) (SetT rhs) = SetT $ (\lhs' rhs' -> Prelude.filter (`notElem` rhs') lhs') <$> lhs <*> rhs

intersection :: (Eq k, Applicative m) => SetT m k -> SetT m k -> SetT m k
intersection (SetT lhs) (SetT rhs) = SetT $ (\lhs' rhs' -> Prelude.filter (`elem` rhs') lhs') <$> lhs <*> rhs

cartesianProduct :: (Applicative m) => SetT m l -> SetT m r -> SetT m (l, r)
cartesianProduct lhs rhs = (,) <$> lhs <*> rhs

disjointUnion :: (Applicative m) => SetT m l -> SetT m r -> SetT m (Either l r)
disjointUnion lhs rhs = fmap Left lhs `union` fmap Right rhs

filter :: (Functor m) => (k -> Bool) -> SetT m k -> SetT m k
filter cond (SetT set) = SetT $ Prelude.filter cond <$> set

filterM :: (Monad m) => (k -> m Bool) -> SetT m k -> SetT m k
filterM cond = toTransformer . fmap fst . partitionM cond

partition :: (Functor m) => (k -> Bool) -> SetT m k -> m (Set k, Set k)
partition cond (SetT set) = set <&> \set' -> bimap (SetT . Identity) (SetT . Identity) $ List.partition cond set'

partitionT :: (Functor m) => (k -> Bool) -> SetT m k -> (SetT m k, SetT m k)
partitionT cond set = (filter cond set, filter (not . cond) set)

partitionM :: (Monad m) => (k -> m Bool) -> SetT m k -> m (Set k, Set k)
partitionM cond (SetT set) =
  set
    >>= fmap
      ( bimap (SetT . Identity . fmap snd) (SetT . Identity . fmap snd)
          . List.partition fst
      )
      . mapM (\v -> (,v) <$> cond v)

partitionMT :: (Monad m) => (k -> m Bool) -> SetT m k -> (SetT m k, SetT m k)
partitionMT cond set = (filterM cond set, filterM (fmap not . cond) set)
