{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

module Hdcc.General.Link
  ( LoaderImplementation (LoaderImplementation),
    LinkedT,
    Linked,
    lookup,
    fetch,
    link,
    intoApplicative,
  )
where

import Control.Monad ((>=>))
import Control.Monad.Trans.Accum (AccumT (), add, evalAccumT, looks)
import Control.Monad.Trans.Class (MonadTrans (lift))
import Data.Functor ((<&>))
import Data.Functor.Identity (Identity (Identity))
import qualified Data.Map as M
import Prelude hiding (lookup)

-- This stage doesn't mean the actual linking stage, but the step of providing an interface for loading the definitions dynamically that are needed for the following stages.

data LinkedThelper name_global name_loader symbol_global m v where
  LinkedThelperReturn :: v -> LinkedThelper name_loader name_global symbol_global m v
  LinkedThelperResolve ::
    name_loader ->
    name_global ->
    (name_loader -> LinkedT name_global name_loader symbol_global m v) ->
    LinkedThelper name_global name_loader symbol_global m v
  LinkedThelperFetch ::
    name_loader ->
    (symbol_global -> LinkedT name_global name_loader symbol_global m v) ->
    LinkedThelper name_global name_loader symbol_global m v

deriving instance (Functor m) => Functor (LinkedThelper name_global name_loader symbol_global m)

-- Globally linked monad (used for actions on files)
newtype LinkedT name_global name_loader symbol_global m v = LinkedT (m (LinkedThelper name_global name_loader symbol_global m v))

type Linked name_global name_loader symbol_global = LinkedT name_global name_loader symbol_global Identity

instance (Functor f) => Functor (LinkedT name_global name_loader symbol_global f) where
  fmap f (LinkedT v) = LinkedT $ v <&> fmap f

instance (Monad m) => Applicative (LinkedT name_global name_loader symbol_global m) where
  pure = LinkedT . pure . LinkedThelperReturn
  lhs <*> rhs = lhs >>= (<$> rhs)

instance (Monad m) => Monad (LinkedT name_global name_loader symbol_global m) where
  LinkedT lhs >>= rhs = LinkedT $ do
    lhs' <- lhs
    case lhs' of
      LinkedThelperReturn v -> case rhs v of LinkedT v' -> v'
      LinkedThelperResolve name_loader name process -> return $ LinkedThelperResolve name_loader name $ process >=> rhs
      LinkedThelperFetch name process -> return $ LinkedThelperFetch name $ process >=> rhs

instance MonadTrans (LinkedT name_global name_loader symbol_global) where
  lift = LinkedT . fmap LinkedThelperReturn

intoApplicative :: (Applicative m) => Linked name_global name_loader symbol_global v -> LinkedT name_global name_loader symbol_global m v
intoApplicative (LinkedT (Identity v)) = LinkedT $ pure $ case v of
  LinkedThelperReturn v' -> LinkedThelperReturn v'
  LinkedThelperResolve name_loader name cont -> LinkedThelperResolve name_loader name $ intoApplicative . cont
  LinkedThelperFetch name cont -> LinkedThelperFetch name $ intoApplicative . cont

lookup :: (Applicative m) => name_loader -> name_global -> LinkedT name_global name_loader symbol_global m name_loader
lookup l n = LinkedT $ pure $ LinkedThelperResolve l n $ LinkedT . pure . LinkedThelperReturn

fetch :: (Applicative m) => name_loader -> LinkedT name_global name_loader symbol_global m symbol_global
fetch l = LinkedT $ pure $ LinkedThelperFetch l $ LinkedT . pure . LinkedThelperReturn

-- Project
data LoaderImplementation name_global symbol_global m i where
  LoaderImplementation ::
    (i -> m symbol_global) -> -- Load
    (i -> name_global -> m i) -> -- Get dependency identifier
    LoaderImplementation name_global symbol_global m i

link ::
  forall m name_loader name_global symbol_global result_final.
  (Monad m, Ord name_global, Ord name_loader) =>
  LoaderImplementation name_global symbol_global m name_loader ->
  LinkedT name_global name_loader symbol_global m result_final ->
  m result_final
link (LoaderImplementation fetch_impl resolve_impl) process = do
  let process_monad ::
        LinkedT name_global name_loader symbol_global m result_temporary ->
        AccumT (M.Map (name_loader, name_global) name_loader, M.Map name_loader symbol_global) m result_temporary
      process_monad (LinkedT instruction) = do
        lift instruction >>= \case
          LinkedThelperReturn v -> return v
          LinkedThelperResolve name_loader name cont ->
            looks (M.lookup (name_loader, name) . fst) >>= \case
              Just r -> process_monad $ cont r
              Nothing -> do
                resolved <- lift $ resolve_impl name_loader name
                add (M.singleton (name_loader, name) resolved, M.empty)
                process_monad $ cont resolved
          LinkedThelperFetch name_loader cont ->
            looks (M.lookup name_loader . snd) >>= \case
              Just r -> process_monad $ cont r
              Nothing -> do
                fetched <- lift $ fetch_impl name_loader
                add (M.empty, M.singleton name_loader fetched)
                process_monad $ cont fetched
  (`evalAccumT` (M.empty, M.empty)) $ process_monad process
