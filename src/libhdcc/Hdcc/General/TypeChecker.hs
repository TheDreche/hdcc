{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE RankNTypes #-}

module Hdcc.General.TypeChecker (Action (embed), ActionT, run, mapActionT, runAction) where

import Control.Monad.Trans.Class (MonadTrans (lift))
import Data.Bifunctor (Bifunctor (bimap))
import Prelude hiding (lookup)

-- Action monad
newtype ActionT a m r = ActionT (m (Either r (a m r)))

instance (Functor (a m), Functor m) => Functor (ActionT a m) where
  fmap f (ActionT c) = ActionT $ bimap f (fmap f) <$> c

instance (Functor (a m), Action a, Monad m) => Applicative (ActionT a m) where
  pure = ActionT . pure . Left
  lhs <*> rhs = lhs >>= (<$> rhs)

class (forall m. (Functor m) => Functor (a m)) => Action a where
  embed :: (Monad m) => a m (ActionT a m r) -> a m r

instance (Functor (a m), Action a, Monad m) => Monad (ActionT a m) where
  ActionT lhs >>= rhs =
    ActionT $
      lhs >>= \case
        Left lhs' -> case rhs lhs' of ActionT c -> c
        Right lhs' -> return $ Right $ embed $ rhs <$> lhs'

instance (Action a) => MonadTrans (ActionT a) where
  lift = ActionT . fmap Left

mapActionT :: (m (Either v (a m v)) -> m' (Either v' (a' m' v'))) -> ActionT a m v -> ActionT a' m' v'
mapActionT f (ActionT c) = ActionT $ f c

run :: (Applicative m) => a m r -> ActionT a m r
run = ActionT . pure . Right

runAction :: (Monad m) => (a m r -> ActionT a m r) -> ActionT a m r -> m r
runAction handler (ActionT action) =
  action >>= \case
    Left v -> pure v
    Right cont -> runAction handler $ handler cont
