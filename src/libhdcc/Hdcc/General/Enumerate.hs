{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Hdcc.General.Enumerate (EnumerateT, Enumerate, genNumber, evalEnumerateT, evalEnumerate) where

import Control.Monad.Trans.Class (MonadTrans)
import Control.Monad.Trans.State (StateT (runStateT), get, modify)
import Data.Functor.Identity (Identity (runIdentity))

newtype EnumerateT m v = EnumerateT (StateT Integer m v) deriving (Functor, Applicative, Monad, MonadTrans)

type Enumerate = EnumerateT Identity

genNumber :: (Monad m) => EnumerateT m Integer
genNumber = EnumerateT $ const <$> get <*> modify (+ 1)

evalEnumerateT :: (Functor m) => EnumerateT m v -> m v
evalEnumerateT (EnumerateT c) = fst <$> runStateT c 0

evalEnumerate :: Enumerate v -> v
evalEnumerate = runIdentity . evalEnumerateT
