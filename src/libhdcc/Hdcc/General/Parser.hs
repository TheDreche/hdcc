{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}

module Hdcc.General.Parser
  ( Correction (Correction),
    File (File),
    Node (Node),
    NodeToken (NodeToken),
    NodeData,
    Parser,
    addCompoundPostfixes,
    genCompoundParser,
    genListParser,
    genListSeparatedParser,
    genMaybeParser,
    genTokenParser,
    runParser,
    unNode,
  )
where

import Control.Applicative (Alternative (empty, (<|>)))
import Control.Monad (MonadPlus, (>=>))
import Data.Bifunctor (Bifunctor (bimap, first, second))
import Data.Either (partitionEithers)
import Data.Foldable (Foldable (toList))
import Data.Functor ((<&>))
import Data.List (map)
import qualified Data.Set as Set
import Hdcc.General.Error (Error)
import qualified Hdcc.General.Lexer as Lexer
import Hdcc.General.Position (Span (Span))
import Hdcc.General.Set (Set, union)
import qualified Hdcc.General.Set as S
import Prelude hiding (filter, map, span)

-- Result
data Correction tt = Correction Span (Maybe tt) deriving (Eq, Ord, Show)

data File e tt v = File (Set.Set ([Correction tt], v)) [Error e] deriving (Eq, Ord, Show)

-- Parser state
data ParserState tt t where
  ParserState ::
    (Lexer.TokenData tt t, Eq t, Show t) =>
    Span ->
    [Lexer.Token tt t] ->
    ParserState tt t

deriving instance Eq (ParserState tt t)

deriving instance Ord (ParserState tt t)

deriving instance Show (ParserState tt t)

-- Parser monad
newtype Parser_ tt t p = Parser_ {unParser_ :: Set (Either (Span, Parser_ tt t p, Span, tt, Parser_ tt t p) (ParserState tt t, p))} deriving (Show, Eq)

instance Functor (Parser_ tt t) where
  fmap f (Parser_ v) = Parser_ $ v <&> bimap (\(a, b, c, d, e) -> (a, fmap f b, c, d, fmap f e)) (second f)

newtype Parser tt t p = Parser {unParser :: ParserState tt t -> Parser_ tt t p}

deriving instance Functor (Parser tt t)

instance Applicative (Parser tt t) where
  pure v = Parser $ Parser_ . S.singleton . Right . (,v)
  a <*> b = do
    a' <- a
    a' <$> b

instance Monad (Parser tt t) where
  Parser lhs >>= rhs =
    let p_ =
          ( >>=
              \case
                Left (s1, Parser_ del, s2, tt, Parser_ add) -> S.singleton $ Left (s1, Parser_ $ p_ del, s2, tt, Parser_ $ p_ add)
                Right (state_new, value) -> unParser_ $ unParser (rhs value) state_new
          )
     in Parser $
          Parser_
            . p_
            . unParser_
            . lhs

instance Alternative (Parser tt t) where
  empty = Parser $ const $ Parser_ empty
  Parser a <|> Parser b =
    Parser $ \state_initial -> Parser_ $ unParser_ (a state_initial) `union` unParser_ (b state_initial)

instance MonadPlus (Parser tt t)

instance MonadFail (Parser tt t) where
  fail _ = Parser $ const $ Parser_ empty

modifyParserState :: (ParserState tt t -> ParserState tt t) -> Parser tt t ()
modifyParserState f = Parser $ \state -> Parser_ $ S.singleton $ Right (f state, ())

advanceParserState :: ParserState tt t -> ParserState tt t
advanceParserState (ParserState (Span _ end) ((Lexer.Token (Span _ start) _) : r)) = ParserState (Span start end) r
advanceParserState (ParserState _ []) = undefined

advance :: (Lexer.TokenData tt t) => Parser tt t ()
advance = modifyParserState $ \case
  (ParserState (Span _ end) ((Lexer.Token (Span _ start) _) : r)) -> ParserState (Span start end) r
  (ParserState _ []) -> undefined

parserState :: Parser tt t (ParserState tt t)
parserState = Parser $ \state -> Parser_ $ S.singleton $ Right (state, state)

currentLexerToken :: Parser tt t (Lexer.Token tt t)
currentLexerToken = do
  (ParserState _ remaining) <- parserState
  case remaining of
    t : _ -> return t
    [] -> fail "current token is EOF"

currentToken :: Parser tt t (Either String t)
currentToken = (\(Lexer.Token _ t) -> t) <$> currentLexerToken

currentPosition :: Parser tt t Span
currentPosition = (\(Lexer.Token p _) -> p) <$> currentLexerToken

remainingSpan :: Parser tt t Span
remainingSpan = do
  (ParserState span _) <- parserState
  return span

runParser :: (Lexer.TokenData tt t, Ord p) => Parser tt t p -> Lexer.File e tt t -> File e tt p
runParser (Parser parser) (Lexer.File span tokens errors) =
  let r :: (Lexer.TokenData tt t) => Parser_ tt t v -> Set ([Correction tt], v)
      r parser' = case partitionEithers
        $ map
          ( \case
              Right (ParserState _ [], v) -> Right v
              Right (ParserState _ (Lexer.Token pos _ : ts), v) -> Left (pos, Parser_ $ S.singleton $ Right (ParserState undefined ts, v), undefined, undefined, Parser_ S.empty)
              Left v -> Left v
          )
        $ toList
        $ unParser_ parser' of
        (_, f : v) -> S.fromList $ (f : v) <&> ([],)
        (list, []) ->
          S.fromList list >>= \(position_rm, remove, _, _, _) ->
            first (Correction position_rm Nothing :) <$> r remove
   in File (S.toSet $ r $ parser $ ParserState span tokens) errors

-- Nodes
class (Eq n, Ord n, Show n) => NodeData n

data Node n where
  Node :: (NodeData n) => Span -> n -> Node n

unNode :: Node n -> n
unNode (Node _ n) = n

deriving instance Eq (Node n)

deriving instance Show (Node n)

deriving instance Ord (Node n)

-- Token nodes
data NodeToken n where
  NodeToken :: (NodeData n) => Span -> Maybe n -> NodeToken n

deriving instance Eq (NodeToken n)

deriving instance Show (NodeToken n)

deriving instance Ord (NodeToken n)

genTokenParser :: (Lexer.TokenData tt t, NodeData n) => tt -> (t -> Maybe n) -> Parser tt t (NodeToken n)
genTokenParser token_type convert = do
  token <- currentToken
  position_rm <- currentPosition
  position_add <- remainingSpan
  maybe
    ( Parser $ \state ->
        Parser_ $
          S.singleton $
            Left
              ( position_rm,
                unParser (genTokenParser token_type convert) $ advanceParserState state,
                position_add,
                token_type,
                Parser_ $ S.singleton $ Right (state, NodeToken position_add Nothing)
              )
    )
    ((<$ advance) . NodeToken position_rm . Just)
    $ convert =<< either (const Nothing) Just token

-- Compound nodes
trackPosition :: (NodeData n) => Parser tt t n -> Parser tt t (Node n)
trackPosition parser = do
  (Span start _) <- currentPosition
  parsed <- parser
  (Span end _) <- remainingSpan
  return $ Node (Span start end) parsed

genCompoundParser :: (NodeData n) => Parser tt t n -> Parser tt t (Node n)
genCompoundParser = trackPosition

addCompoundPostfixes :: (Lexer.TokenData tt t, NodeData n) => Parser tt t (Node n) -> [Node n -> Parser tt t n] -> Parser tt t (Node n)
addCompoundPostfixes parser postfixes =
  foldr
    ((<|>) . (trackPosition . (parser >>=) >=> (`addCompoundPostfixes` postfixes) . return))
    parser
    postfixes

-- Maybe
genMaybeParser :: Parser tt t n -> Parser tt t (Maybe n)
genMaybeParser parser = fmap Just parser <|> return Nothing

-- List
genListParser :: (Lexer.TokenData tt t) => Parser tt t n -> Parser tt t [n]
genListParser item =
  return [] <|> do
    item' <- item
    remainder <- genListParser item
    return $ item' : remainder

genListSeparatedParser :: (Lexer.TokenData tt t) => Parser tt t n -> Parser tt t s -> Parser tt t [n]
genListSeparatedParser item separator =
  return [] <|> do
    item' <- item
    return [item'] <|> do
      _ <- separator
      remainder <- genListSeparatedParser item separator
      return $ item' : remainder
