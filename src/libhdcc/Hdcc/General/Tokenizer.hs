{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}

module Hdcc.General.Tokenizer
  ( File (File),
    TokenData,
    Token (Token),
    genTokenizer,
  )
where

import Control.Monad.Loops (whileM)
import Control.Monad.Trans.Class (MonadTrans (lift))
import Control.Monad.Trans.State (StateT, evalStateT, get, put)
import Control.Monad.Trans.Writer (Writer, runWriter, tell)
import Data.Bifunctor (Bifunctor (second))
import Data.Functor ((<&>))
import Data.Text (Text)
import qualified Data.Text as T
import Hdcc.General.Error (Error (Error), ErrorData)
import Hdcc.General.Position (Position (Position), Span (Span))

data File e t = File Span [Token t] [Error e]

deriving instance (Show (Error e)) => Show (File e t)

deriving instance (Eq (Error e)) => Eq (File e t)

deriving instance (Ord (Error e), Ord (Token t)) => Ord (File e t)

class (Eq t, Show t) => TokenData t

data Token t where
  Token :: (TokenData t) => Span -> Either String t -> Token t

deriving instance Eq (Token t)

deriving instance Show (Token t)

genTokenizer ::
  forall e t.
  (TokenData t, ErrorData e) =>
  [Text -> Maybe (t, Integer, [e])] ->
  Text ->
  File e t
genTokenizer tokenizers source =
  let next :: Text -> Either String (t, Integer, [e])
      next remaining =
        foldr
          (\i o -> maybe o Right $ i remaining)
          ( Left
              ( either
                  (T.head remaining :)
                  (const [T.head remaining])
                  $ let r = T.tail remaining
                     in if T.null r
                          then Left ""
                          else next r
              )
          )
          tokenizers
      lexToken :: StateT Position (Writer [Error e]) (Token t)
      lexToken = do
        token_start@(Position start) <- get
        let remainder = T.drop (fromInteger start) source
            token_data = next remainder
            token_end = Position $ start + either (toInteger . length) (\(_, v, _) -> v) token_data
            token_errors = either (const []) (\(_, _, e) -> e) token_data
        put token_end
        lift $ tell $ token_errors <&> Error (Span token_start token_end)
        return $ Token (Span token_start token_end) $ second (\(v, _, _) -> v) token_data
   in uncurry (File (Span (Position 0) (Position $ toInteger $ T.length source))) $
        runWriter $
          evalStateT
            ( whileM
                ((\(Position pos) -> T.compareLength source (fromInteger pos) == GT) <$> get)
                lexToken
            )
            (Position 0)
