{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}

module Hdcc.General.Recursion
  ( RecurseDetectT (RecurseDetectT),
    RecurseDetect,
    withinScope,
    runRecurseDetectT,
    mapRecurseDetectT,
    withinScopeMEq,
  )
where

import Control.Applicative (Alternative ())
import Control.Monad (MonadPlus ())
import Control.Monad.Fix (MonadFix ())
import Control.Monad.IO.Class (MonadIO ())
import Control.Monad.Trans.Class (MonadTrans ())
import Control.Monad.Trans.Reader (ReaderT (ReaderT, runReaderT), ask, mapReaderT, withReaderT)
import Data.Functor.Identity (Identity ())

newtype RecurseDetectT s m v = RecurseDetectT (ReaderT [s] m v) deriving (Functor, Applicative, Monad, MonadTrans, MonadFail, MonadFix, MonadIO, Alternative, MonadPlus)

type RecurseDetect s = RecurseDetectT s Identity

withinScope :: (Eq s, Applicative m) => s -> RecurseDetectT s m r -> RecurseDetectT s m (Maybe r)
withinScope s (RecurseDetectT a) =
  RecurseDetectT $
    ReaderT $ \stack ->
      if s `elem` stack
        then pure Nothing
        else Just <$> runReaderT a (s : stack)

-- This function is for using this monad for recursion detection in LinkedT
withinScopeMEq :: (Monad m) => t -> (t -> t -> ReaderT [t] m Bool) -> RecurseDetectT t m (Maybe a) -> RecurseDetectT t m (Maybe a)
withinScopeMEq s eq (RecurseDetectT r) = RecurseDetectT $ do
  let checkContains _ [] = pure False
      checkContains i (l : ls) =
        eq i l >>= \case
          True -> pure True
          False -> checkContains i ls
  stack <- ask
  already <- checkContains s stack
  if already
    then pure Nothing
    else withReaderT (s :) r

runRecurseDetectT :: RecurseDetectT s m r -> m r
runRecurseDetectT (RecurseDetectT c) = runReaderT c []

mapRecurseDetectT :: (m a -> n b) -> RecurseDetectT s m a -> RecurseDetectT s n b
mapRecurseDetectT f (RecurseDetectT c) = RecurseDetectT $ mapReaderT f c
