{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}

module Main.Diagnostic
  ( Diagnostic (..),
    Location (Location, lPath, lStart, lEnd),
    Severity (..),
    Type (..),
    diagnosticSeverity,
    diagnosticType,
  )
where

-- Utility types
data Location = Location {lPath :: FilePath, lStart :: Integer, lEnd :: Integer}
  deriving (Eq, Ord, Show)

-- Diagnostic: Translatable and afterwards formattable (as diagnostic message)
data Diagnostic
  = DGeneralLMissingNewlineAtEndOfFile FilePath
  | DGeneralLUnrecognizedCharacter Location
  | DDmlLUnterminatedBlockComment FilePath Integer -- path, start
  deriving (Eq, Ord, Show)

-- Diagnostic Type: Explainable
data Type
  = TGeneralLMissingNewlineAtEndOfFile
  | TGeneralLUnrecognizedCharacter
  | TDmlLUnterminatedBlockComment
  deriving (Eq, Ord, Show, Bounded, Enum)

diagnosticType :: Diagnostic -> Type
diagnosticType (DGeneralLMissingNewlineAtEndOfFile _) = TGeneralLMissingNewlineAtEndOfFile
diagnosticType (DGeneralLUnrecognizedCharacter _) = TGeneralLUnrecognizedCharacter
diagnosticType (DDmlLUnterminatedBlockComment _ _) = TDmlLUnterminatedBlockComment

-- Diagnostic Severity
data Severity = SWarning | SError deriving (Eq, Ord, Show, Bounded, Enum)

diagnosticSeverity :: Type -> Severity
diagnosticSeverity TGeneralLMissingNewlineAtEndOfFile = SWarning
diagnosticSeverity TGeneralLUnrecognizedCharacter = SError
diagnosticSeverity TDmlLUnterminatedBlockComment = SError
