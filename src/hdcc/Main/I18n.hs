{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Main.I18n (Localized (translate), LocalizeT, Localize, Message (..), runLocalizeT, runLocalize) where

import Control.Applicative (Alternative)
import Control.Monad (MonadPlus)
import Control.Monad.Fix (MonadFix)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Identity (Identity (runIdentity))
import Control.Monad.Trans (MonadTrans)
import Control.Monad.Trans.Reader (ReaderT (runReaderT), asks)
import Control.Monad.Zip (MonadZip)
import Data.Functor ((<&>))
import Data.Gettext (Catalog, gettext)
import Data.String (IsString (fromString))
import Data.Text.Format (Only (Only), format)
import Data.Text.Format.Params (Params)
import qualified Data.Text.Lazy as T
import GHC.Generics (Generic)

data Message
  = MError
  | MWarning
  | MMissingNewlineAtEndOfFile
  | MFilesShouldEndInNewline
  | MUnrecognizedCharacter
  | MInvalidCharacterInSourceCode Char
  | MUnterminatedBlockComment
  | MBlockCommentsHaveToBeTerminated

class Localized m where
  translate :: Message -> m T.Text

-- The catalog contains all translations of the current language; having fallback languages is not supported
newtype LocalizeT m v = LocalizeT (ReaderT (Maybe Catalog) m v)
  deriving
    ( MonadTrans,
      MonadFail,
      MonadFix,
      MonadIO,
      MonadZip,
      Alternative,
      Applicative,
      Functor,
      Monad,
      MonadPlus,
      Generic
    )

type Localize = LocalizeT Identity

runLocalizeT :: LocalizeT m v -> Maybe Catalog -> m v
runLocalizeT (LocalizeT v) = runReaderT v

runLocalize :: Localize v -> Maybe Catalog -> v
runLocalize = (runIdentity .) . runLocalizeT

__ :: (Monad m) => String -> LocalizeT m T.Text
__ s = LocalizeT $ asks $ maybe (fromString s) (`gettext` fromString s)

__f :: (Monad m, Params p) => String -> p -> LocalizeT m T.Text
__f s f = __ s <&> (`format` f) . fromString . T.unpack

instance (Monad m) => Localized (LocalizeT m) where
  translate MError = __ "Error"
  translate MWarning = __ "Warning"
  translate MMissingNewlineAtEndOfFile = __ "Missing newline at end of file"
  translate MFilesShouldEndInNewline = __ "Files should always end in a new line"
  translate MUnrecognizedCharacter = __ "Unrecognized character"
  translate (MInvalidCharacterInSourceCode c) = __f "Character {} must not appear in the source code" $ Only c
  translate MUnterminatedBlockComment = __ "Unterminated block comment"
  translate MBlockCommentsHaveToBeTerminated = __ "Block comments must be terminated"
