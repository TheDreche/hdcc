{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}

module Main.DiagnosticI18n
  ( Correction (Correction),
    Diagnostic (Diagnostic),
    Hint (Hint),
    PositionedMessage (PositionedMessage),
    internationalize,
  )
where

import Data.Functor ((<&>))
import Data.Text (Text, index)
import qualified Data.Text as T
import Main.Diagnostic (Location (Location, lPath), Severity, diagnosticSeverity, diagnosticType)
import qualified Main.Diagnostic as D
import Main.I18n (Message (MBlockCommentsHaveToBeTerminated, MFilesShouldEndInNewline, MInvalidCharacterInSourceCode, MMissingNewlineAtEndOfFile, MUnrecognizedCharacter, MUnterminatedBlockComment))

data PositionedMessage = PositionedMessage Location Message Text

positionMessage :: (Applicative m) => (FilePath -> m Text) -> Message -> Location -> m PositionedMessage
positionMessage content = positionContentMessage content . const

positionContentMessage :: (Applicative m) => (FilePath -> m Text) -> (Text -> Message) -> Location -> m PositionedMessage
positionContentMessage content message loc@(Location {lPath = path}) = content path <&> \c -> PositionedMessage loc (message c) c

data Diagnostic = Diagnostic Message Severity PositionedMessage [Hint]

newtype Hint = Hint PositionedMessage

data Correction = Correction Location Text

internationalize :: (Applicative m) => (FilePath -> m Text) -> D.Diagnostic -> m Diagnostic
internationalize content diagnostic@(D.DGeneralLUnrecognizedCharacter location@(Location _ start _)) =
  Diagnostic MUnrecognizedCharacter (diagnosticSeverity $ diagnosticType diagnostic)
    <$> positionContentMessage content (MInvalidCharacterInSourceCode . (`index` fromInteger start)) location
    <*> pure []
internationalize content diagnostic@(D.DGeneralLMissingNewlineAtEndOfFile path) =
  content path <&> \c ->
    Diagnostic
      MMissingNewlineAtEndOfFile
      (diagnosticSeverity $ diagnosticType diagnostic)
      ( PositionedMessage
          (Location path (toInteger $ T.length c) (toInteger $ T.length c))
          MFilesShouldEndInNewline
          c
      )
      []
internationalize content diagnostic@(D.DDmlLUnterminatedBlockComment path start) =
  content path <&> \c ->
    Diagnostic
      MUnterminatedBlockComment
      (diagnosticSeverity $ diagnosticType diagnostic)
      ( PositionedMessage
          (Location path start $ toInteger $ T.length c)
          MBlockCommentsHaveToBeTerminated
          c
      )
      []
