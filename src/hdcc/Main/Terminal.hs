{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE LambdaCase #-}

module Main.Terminal (hSupportsAnsi, hWindow) where

import System.Console.Terminal.Size (Window, hSize)
import System.Environment (lookupEnv)
import System.IO (Handle, hIsTerminalDevice)
import Prelude hiding (putStr)

-- Taken from pretty-terminal-0.1.0.0:System.Console.Pretty.supportsPretty
hSupportsAnsi :: Handle -> IO Bool
hSupportsAnsi h = (&&) <$> hIsTerminalDevice h <*> (not <$> isDumb)
  where
    isDumb = (== Just "dumb") <$> lookupEnv "TERM"

hWindow :: (Integral n) => Handle -> IO (Maybe (Window n))
hWindow h = hSupportsAnsi h >>= \case True -> hSize h; False -> return Nothing
