{- hdcc is Dreches personal test compiler, mostly for his own programming
 - languages.
 -
 - Copyright (C) 2023, 2024  Dreche
 -
 - This program is free software: you can redistribute it and/or modify
 - it under the terms of the GNU General Public License as published by
 - the Free Software Foundation, either version 3 of the License, or
 - (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program.  If not, see <https://www.gnu.org/licenses/>.
 -}
{-# LANGUAGE LambdaCase #-}

module Main (main) where

import Control.Applicative (Alternative (some, (<|>)), optional)
import Data.Maybe (fromMaybe)
import qualified Data.Set as Set
import Data.String (IsString (fromString))
import Data.Text.IO (readFile)
import qualified Data.Version
import Hdcc.Dml.Link (GlobalIdentifier (GlobalIdentifier))
import Options.Applicative
  ( Parser,
    action,
    customExecParser,
    disambiguate,
    flag',
    help,
    helper,
    info,
    long,
    metavar,
    prefs,
    progDesc,
    short,
    strArgument,
    strOption,
    value,
  )
import System.FilePath (dropExtension, splitPath)
import Prelude hiding (lex, readFile)

-- Metadata
programVersion :: Data.Version.Version
programVersion = Data.Version.makeVersion [0, 1, 0, 0]

versionInformation :: String
versionInformation = "hdcc (hdcc) " ++ Data.Version.showVersion programVersion

-- Cli parsing
data ExecutionMode = ECompile | EJIT | EInterpret

cliExecutionMode :: Parser ExecutionMode
cliExecutionMode =
  fmap (fromMaybe ECompile) $
    optional $
      flag' EJIT (long "jit" <> help "Use Just-In-Time-Compilation instead of Ahead-Of-Time-Compilation.")
        <|> flag' EInterpret (long "interpret" <> help "Interpret code instead of compile.")

data CliCommand = CVersion | CCompile [FilePath] ExecutionMode (Maybe FilePath) String

cliParser :: Parser CliCommand
cliParser =
  flag' CVersion (short 'V' <> long "version" <> help "Show version information.")
    <|> CCompile
      <$> some (strArgument $ metavar "FILES..." <> action "file")
      <*> cliExecutionMode
      <*> optional (strOption $ long "out" <> metavar "FILE" <> action "file" <> help "Output file.")
      <*> strOption (long "entry" <> metavar "ENTRY_POINT" <> help "Entry point." <> value "main")

main :: IO ()
main = do
  customExecParser (prefs disambiguate) (info (helper <*> cliParser) $ progDesc "Compiler collection for dreches custom languages.") >>= \case
    CVersion -> putStrLn versionInformation
    CCompile files execution_mode output_file entry_point -> do
      mapM_
        ( \file -> do
            let entry = (Set.singleton ".", GlobalIdentifier $ fromString . dropExtension <$> splitPath file)
            contents <- readFile file
            putStrLn $ file ++ ":"
            print contents
        )
        files
      putStrLn "Compiling has not been implemented up to now ..."
