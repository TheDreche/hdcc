let
	sources = import ./sources.nix;
	pkgs = import sources.nixpkgs { };
in

# See https://docs.haskellstack.org/en/stable/nix_integration/#using-a-custom-shellnix-file
#{ ghc }:

# Stack doesn't know the nixpkgs name of the latest version
let ghc = pkgs.haskell.compiler.ghc96; in

pkgs.haskell.lib.buildStackProject {
	inherit ghc;
	name = "haskell-stack-nix";

	# System dependencies needed at compilation time
	buildInputs = [
		pkgs.icu
	];
}
